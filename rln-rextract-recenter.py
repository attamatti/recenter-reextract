#!/usr/bin/python


### reextract particles from a starfile - create new autopick style starfile
### optionally apply relion's particles shifts to the original boxes to generate particles that are better centered

import sys

#-----------------------------------------------------------------------------------#
class Arg(object):
    _registry = []
    def __init__(self, flag, value, req):
        self._registry.append(self)
        self.flag = flag
        self.value = value
        self.req = req

def make_arg(flag, value, req):
    errormsg = 'USAGE: rln-rextract-recenter.py --i <input star file> --o <output suffix> --recenter\n the --recenter flag is optional if you want to recenter the particles'
    Argument = Arg(flag, value, req)
    if Argument.req == True:
        if Argument.flag not in sys.argv:
            print(errormsg)
            sys.exit("ERROR: required argument '{0}' is missing".format(Argument.flag))
    if Argument.value == True:
        try:
            test = sys.argv[sys.argv.index(Argument.flag)+1]
        except ValueError:
            if Argument.req == True:
                print(errormsg)
                sys.exit("ERROR: required argument '{0}' is missing".format(Argument.flag))
            elif Argument.req == False:
                return False
        except IndexError:
                print(errormsg)
                sys.exit("ERROR: argument '{0}' requires a value".format(Argument.flag))
        else:
            if Argument.value == True:
                Argument.value = sys.argv[sys.argv.index(Argument.flag)+1]
        
    if Argument.value == False:
        if Argument.flag in sys.argv:
            Argument.value = True
        else:
            Argument.value = False
    return Argument.value
#-----------------------------------------------------------------------------------#


recenter = make_arg('--recenter',False,False)
inputstar = make_arg('--i',True,True)
suffix = make_arg('--o',True,True)



## get the relion file and read the data
relionfile = open(inputstar, "r")
odata = relionfile.readlines()

## find the labels and identify the right columns; put in labeldic
vers = "1.0"
print ("**** particle rextraction and recentereing v {0} ****".format(vers))
print ("Matt Iadanza - Astbury Centre for Structural Molecular Biology")
print ("University of Leeds")


labelnames = ("_rlnCoordinateX","_rlnCoordinateY","_rlnOriginX","_rlnOriginY","_rlnMicrographName")
labeldic = {}
for i in odata:
    if any(s in i for s in labelnames):
        labeldic[i.split()[0]] = int(i.split()[1].strip('#'))  

# put all of the data into an array
data = []
for i in odata:
    if len(i.split()) > 4: 
        data.append(i.split())

# find the filenames, make a list of them
filenames = []
for i in data:
    if i[labeldic["_rlnMicrographName"]-1] not in filenames:
        filenames.append(i[labeldic["_rlnMicrographName"]-1])

# make a starfile for each original file with the new corrdinates

for i in filenames:
    writename = i.split('.')[0]
    fileout = open("{0}{1}.star".format(writename,suffix),"w")
    fileout.write("\ndata_\n\nloop_\n_rlnCoordinateX #1\n_rlnCoordinateY #2\n")
    for j in data:
        if j[labeldic["_rlnMicrographName"]-1] == i:
            if recenter == True:
                newx = float(j[labeldic["_rlnCoordinateX"]-1]) - float(j[labeldic["_rlnOriginX"]-1])
                newy = float(j[labeldic["_rlnCoordinateY"]-1]) - float(j[labeldic["_rlnOriginY"]-1])
                fileout.write("{0}\t{1}\n".format(newx,newy)) 
            else:
                fileout.write("{0}\t{1}\n".format(j[labeldic["_rlnCoordinateX"]-1],j[labeldic["_rlnCoordinateY"]-1])) 
    print ("wrote file:  {0}{1}.star".format(writename,suffix))
